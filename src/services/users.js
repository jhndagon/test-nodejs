function usersService(userModel, rolesModel) {
  async function findAll() {
    return (await userModel.findAll({ include: rolesModel })) || [];
  }

  async function create(user) {
    const userToSave = userModel.build(user);
    await userToSave.save();
    return userToSave;
  }

  async function deleteById(id) {
    const userDeleted = await userModel.destroy({ where: { id } });
    return userDeleted > 0;
  }

  async function asignRoleToUser(userId, roleId) {
    const userUpdate = await userModel.findOne({ where: { id: userId } });
    const role = await rolesModel.findOne({ where: { id: roleId } });
    if (!userUpdate || !role) {
      return null;
    }
    userUpdate.roles_id = roleId;
    await userUpdate.save();
    return userUpdate;
  }

  return {
    findAll,
    create,
    deleteById,
    asignRoleToUser,
  };
}

module.exports = {
  usersService,
};
