const { productsService } = require('./products');
const { usersService } = require('./users');
const { rolesService } = require('./roles');
const { salesService } = require('./sales');

module.exports = {
  productsService,
  usersService,
  rolesService,
  salesService,
};
