function salesService(salesModel, productModel, userModel, Op) {
  async function findAll() {
    return (await salesModel.findAll({ include: [productModel] })) || [];
  }
  async function create(sales) {
    const product = await productModel.findOne({
      where: { id: sales.products_id },
    });
    const user = await userModel.findOne({
      where: { id: sales.users_id },
    });
    if (!product || !user) {
      return null;
    }
    let salesToSave = salesModel.build(sales);
    salesToSave = await salesToSave.save();
    return salesToSave;
  }

  async function deleteById(id) {
    const salesDeleted = await salesModel.destroy({ where: { id } });
    return salesDeleted > 0;
  }

  async function updateById(id, sale) {
    let product;
    let user;
    if (sale.products_id) {
      product = await productModel.findOne({
        where: { id: sale.products_id },
      });
    }
    if (sale.users_id) {
      user = await userModel.findOne({
        where: { id: sale.users_id },
      });
    }
    if (
      product !== undefined &&
      user !== undefined &&
      (product == null || user == null)
    ) {
      return null;
    }
    const salesToUpdate = await salesModel.findOne({ where: { id } });
    if (!salesToUpdate) {
      return null;
    }
    Object.keys(sale).forEach((key) => {
      salesToUpdate[key] = sale[key];
    });
    await salesToUpdate.save();
    return salesToUpdate;
  }

  async function dailyBalance(day) {
    const actual = new Date();
    const next = new Date();
    if (day) {
      actual.setDate(day);
      next.setDate(day);
    }
    next.setDate(next.getDate() + 1);
    const sales = await salesModel.findAll({
      where: {
        sales_at: {
          [Op.gte]: `${actual.getFullYear()}-${
            actual.getMonth() + 1
          }-${actual.getDate()}`,
          [Op.lt]: `${next.getFullYear()}-${
            next.getMonth() + 1
          }-${next.getDate()}`,
        },
      },
      include: [productModel],
    });
    const total = sales.map((sale) => sale.qty * sale.product.price);
    return total.length > 0 ? total.reduce((a, b) => a + b) : 0;
  }

  async function monthlyBalance(month) {
    const actual = new Date();
    if (month) {
      actual.setMonth(month - 1);
    }
    const next = new Date(actual.getFullYear(), actual.getMonth() + 1, 1);
    const sales = await salesModel.findAll({
      where: {
        sales_at: {
          [Op.gte]: `${actual.getFullYear()}-${actual.getMonth() + 1}-1`,
          [Op.lt]: `${next.getFullYear()}-${next.getMonth() + 1}-1`,
        },
      },
      include: [productModel],
    });
    const total = sales.map((sale) => sale.qty * sale.product.price);
    return total.length > 0 ? total.reduce((a, b) => a + b) : 0;
  }

  return {
    findAll,
    create,
    deleteById,
    updateById,
    dailyBalance,
    monthlyBalance,
  };
}

module.exports = {
  salesService,
};
