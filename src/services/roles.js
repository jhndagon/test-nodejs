function rolesService(rolesModel) {
  async function create(role) {
    const roleInDb = await rolesModel.findOne({ where: { name: role.name } });
    if (roleInDb) {
      return null;
    }
    const roleToSave = rolesModel.build(role);
    await roleToSave.save();
    return roleToSave;
  }

  return {
    create,
  };
}

module.exports = {
  rolesService,
};
