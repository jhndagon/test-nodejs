function productsService(productModel) {
  async function findAll() {
    return (await productModel.findAll()) || [];
  }

  async function create(product) {
    const productToSave = productModel.build(product);
    await productToSave.save();
    return productToSave;
  }

  return {
    findAll,
    create,
  };
}

module.exports = {
  productsService,
};
