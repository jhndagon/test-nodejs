function userNotAutorized(message, status) {
  return {
    error: message,
    status,
    data: null,
  };
}

module.exports = {
  userNotAutorized,
};
