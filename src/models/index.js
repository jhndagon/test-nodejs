const ProductModel = require('./products');
const UserModel = require('./users');
const RolesModel = require('./roles');
const SalesModel = require('./sales');

module.exports = {
  ProductModel,
  UserModel,
  RolesModel,
  SalesModel,
};
