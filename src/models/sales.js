module.exports = (sequelize, DataTypes, Product, User) => {
  const Sales = sequelize.define(
    'sales',
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false,
      },
      qty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      products_id: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      users_id: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sales_at: {
        type: 'TIMESTAMP',
      },
    },
    {
      timestamps: false,
    },
  );
  Sales.belongsTo(Product, {
    foreignKey: 'products_id',
  });
  Sales.belongsTo(User, {
    foreignKey: 'users_id',
  });
  return Sales;
};
