module.exports = (sequelize, DataTypes) => {
  const Roles = sequelize.define(
    'roles',
    {
      id: {
        primaryKey: true,
        type: DataTypes.STRING,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: false,
    },
  );
  return Roles;
};
