const Joi = require('joi');

const salesCreateSchema = Joi.object().keys({
  products_id: Joi.string().guid({ version: 'uuidv4' }).required(),
  users_id: Joi.string().guid({ version: 'uuidv4' }).required(),
  qty: Joi.number().integer().required(),
});

function validateSaleCreateSchema(req, res, next) {
  const result = salesCreateSchema.validate(req.body);
  if (result.error != null) {
    // next(result.error);
    res.status(422).json({
      message: 'Invalid data',
      status: 422,
      description: result.error,
    });
    return;
  }
  next();
}

module.exports = {
  validateSaleCreateSchema,
};
