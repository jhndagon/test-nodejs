const Joi = require('joi');

const userCreateSchema = Joi.object().keys({
  document: Joi.string().max(20).required(),
  last_name: Joi.string().max(30).required(),
  name: Joi.string().max(30).required(),
  roles_id: Joi.string().guid({ version: 'uuidv4' }).required(),
});

function validateUserCreateSchema(req, res, next) {
  const result = userCreateSchema.validate(req.body);
  if (result.error != null) {
    // next(result.error);
    res.status(422).json({
      message: 'Invalid data',
      status: 422,
      description: result.error,
    });
    return;
  }
  next();
}

module.exports = {
  validateUserCreateSchema,
};
