const Joi = require('joi');

const productCreateSchema = Joi.object().keys({
  description: Joi.string().max(30).required(),
  name: Joi.string().max(30).required(),
  price: Joi.number().integer().required(),
});

function validateProductCreateSchema(req, res, next) {
  const result = productCreateSchema.validate(req.body);
  if (result.error != null) {
    // next(result.error);
    res.status(422).json({
      message: 'Invalid data',
      status: 422,
      description: result.error,
    });
    return;
  }
  next();
}

module.exports = {
  validateProductCreateSchema,
};
