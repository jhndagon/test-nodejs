const { validateUserCreateSchema } = require('./users');
const { validateRoleCreateSchema } = require('./roles');
const { validateProductCreateSchema } = require('./products');
const { validateSaleCreateSchema } = require('./sales');

module.exports = {
  validateUserCreateSchema,
  validateRoleCreateSchema,
  validateProductCreateSchema,
  validateSaleCreateSchema,
};
