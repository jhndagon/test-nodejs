const Joi = require('joi');

const roleCreateSchema = Joi.object().keys({
  name: Joi.string().max(30).required(),
});

function validateRoleCreateSchema(req, res, next) {
  const result = roleCreateSchema.validate(req.body);
  if (result.error != null) {
    // next(result.error);
    res.status(422).json({
      message: 'Invalid data',
      status: 422,
      description: result.error,
    });
    return;
  }
  next();
}

module.exports = {
  validateRoleCreateSchema,
};
