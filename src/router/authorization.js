const { userNotAutorized } = require('../models/userNotAuthorized');

function authorization(userModel, roleModel) {
  async function validateRole(userId, role) {
    if (!userId) {
      return userNotAutorized('We hope and user id in auth header', 401);
    }
    const user = await userModel.findOne({
      where: { id: userId },
      include: roleModel,
    });
    if (!user || !user.role.name || user.role.name !== role) {
      return userNotAutorized('Not enough permisions', 403);
    }
    return undefined;
  }
  async function checkAdmin(req, res, next) {
    const userId = req.headers.auth;
    const error = await validateRole(userId, 'admin');
    next(error);
  }

  async function checkEmployee(req, res, next) {
    const userId = req.headers.auth;
    const error = await validateRole(userId, 'employee');
    next(error);
  }

  async function checkEveryone(req, res, next) {
    const userId = req.headers.auth;
    if (!userId) {
      next(userNotAutorized('We hope and user id in auth header', 401));
      return;
    }
    const user = await userModel.findOne({
      where: { id: userId },
      include: roleModel,
    });
    if (
      !user ||
      !user.role.name ||
      (user.role.name !== 'admin' && user.role.name !== ' employee')
    ) {
      next(userNotAutorized('Not enough permisions', 403));
      return;
    }
    next();
  }

  return {
    checkAdmin,
    checkEmployee,
    checkEveryone,
  };
}

module.exports = {
  authorization,
};
