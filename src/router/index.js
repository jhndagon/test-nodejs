const {
  ProductModel,
  UserModel,
  RolesModel,
  SalesModel,
} = require('../models');
const {
  productsService,
  usersService,
  rolesService,
  salesService,
} = require('../services');
const {
  productsController,
  usersController,
  rolesController,
  salesController,
} = require('../controllers');
const {
  validateUserCreateSchema,
  validateRoleCreateSchema,
  validateProductCreateSchema,
  validateSaleCreateSchema,
} = require('./validations');

const { authorization } = require('./authorization');
const { handlingUserNotAuthorized } = require('../controllers/errors');

function route(router, sequelize, DateTypes, operators) {
  const Product = ProductModel(sequelize, DateTypes);
  const productService = productsService(Product);
  const productController = productsController(productService);

  const Roles = RolesModel(sequelize, DateTypes);
  const roleService = rolesService(Roles);
  const roleController = rolesController(roleService);

  const User = UserModel(sequelize, DateTypes, Roles);
  const userService = usersService(User, Roles);
  const userController = usersController(userService);

  const Sales = SalesModel(sequelize, DateTypes, Product, User);
  const saleService = salesService(Sales, Product, User, operators);
  const saleController = salesController(saleService);

  const authorization1 = authorization(User, Roles);

  router.get(
    '/products',
    authorization1.checkEveryone,
    productController.findAll
  );
  router.post(
    '/products',
    authorization1.checkAdmin,
    validateProductCreateSchema,
    productController.create
  );

  router.get('/users', authorization1.checkAdmin, userController.findAll);
  router.post(
    '/users',
    authorization1.checkAdmin,
    validateUserCreateSchema,
    userController.create
  );
  router.delete(
    '/users/:id',
    authorization1.checkAdmin,
    userController.deleteById
  );
  router.put(
    '/users/:userId/roles/:roleId',
    authorization1.checkAdmin,
    userController.asignRoleToUser
  );

  router.post(
    '/roles',
    authorization1.checkAdmin,
    validateRoleCreateSchema,
    roleController.create
  );

  router.get('/sales', authorization1.checkEveryone, saleController.findAll);
  router.post(
    '/sales',
    authorization1.checkEveryone,
    validateSaleCreateSchema,
    saleController.create
  );
  router.delete(
    '/sales/:id',
    authorization1.checkAdmin,
    saleController.deleteById
  );
  router.put(
    '/sales/:id',
    authorization1.checkAdmin,
    saleController.updateById
  );

  router.get(
    '/sales/daily-balance',
    authorization1.checkAdmin,
    saleController.dailyBalance
  );
  router.get(
    '/sales/monthly-balance',
    authorization1.checkAdmin,
    saleController.monthlyBalance
  );

  router.use(handlingUserNotAuthorized);

  return router;
}

module.exports = {
  route,
};
