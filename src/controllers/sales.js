const { uuid } = require('uuidv4');

function salesController(salesService) {
  async function findAll(req, res) {
    const sales = await salesService.findAll();
    return res.status(200).send(sales);
  }

  async function create(req, res) {
    const sales = req.body;
    sales.id = uuid();
    const salesInDb = await salesService.create(sales);
    if (!salesInDb) {
      return res.status(404).json({
        status: 404,
        message: `The user with id ${sales.users_id} or the product with id ${sales.products_id} does not exist in database.`,
      });
    }
    return res.status(201).json(sales);
  }

  async function deleteById(req, res) {
    const { id } = req.params;
    const deleted = await salesService.deleteById(id);
    if (!deleted) {
      res.status(404).json({
        message: `Sale with id ${id} does not exist.`,
        status: 404,
      });
      return;
    }
    res.status(204).end();
  }

  async function updateById(req, res) {
    const sale = req.body;
    const { id } = req.params;
    const salesInDb = await salesService.updateById(id, sale);
    if (!salesInDb) {
      return res.status(404).json({
        status: 404,
        message: `The user with id ${sale.users_id} or the product with id ${sale.products_id} does not exist in database.`,
      });
    }
    return res.status(200).json(salesInDb);
  }

  async function dailyBalance(req, res) {
    const { day } = req.query;
    if (day && (day < 1 || day > 31)) {
      return res.status(400).json({
        status: 400,
        message: 'Day should be between 1-31',
      });
    }
    const total = await salesService.dailyBalance(day);

    return res.status(200).json({ total });
  }

  async function monthlyBalance(req, res) {
    const { month } = req.query;
    if (month && (month < 1 || month > 12)) {
      return res.status(400).json({
        status: 400,
        message: 'Month should be between 1-12',
      });
    }

    const total = await salesService.monthlyBalance(month);

    return res.status(200).json({ total });
  }

  return {
    findAll,
    create,
    deleteById,
    updateById,
    dailyBalance,
    monthlyBalance,
  };
}

module.exports = {
  salesController,
};
