const { productsController } = require('./products');
const { usersController } = require('./users');
const { rolesController } = require('./roles');
const { salesController } = require('./sales');

module.exports = {
  productsController,
  usersController,
  rolesController,
  salesController,
};
