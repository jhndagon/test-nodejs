const { uuid } = require('uuidv4');

function productsController(productService) {
  async function findAll(req, res) {
    const products = await productService.findAll();
    res.status(200).send(products);
  }

  async function create(req, res) {
    const product = req.body;
    product.id = uuid();
    await productService.create(product);
    return res.status(201).json(product);
  }

  return {
    findAll,
    create,
  };
}

module.exports = {
  productsController,
};
