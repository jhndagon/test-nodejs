function handlingUserNotAuthorized(err, req, res, next) {
  if (err) {
    console.log(err);
    res.status(err.status).json(err);
  } else {
    next();
  }
}

module.exports = {
  handlingUserNotAuthorized,
};
