const { uuid } = require('uuidv4');

function usersController(userService) {
  async function findAll(req, res) {
    const users = await userService.findAll();
    return res.status(200).send(users);
  }

  async function create(req, res) {
    const user = req.body;
    user.id = uuid();
    await userService.create(user);
    return res.status(201).json(user);
  }

  async function deleteById(req, res) {
    const { id } = req.params;
    const deleted = await userService.deleteById(id);
    if (!deleted) {
      res.status(404).json({
        message: `User with id ${id} does not exist.`,
        status: 404,
      });
      return;
    }
    res.status(204).end();
  }

  async function asignRoleToUser(req, res) {
    const { userId, roleId } = req.params;
    const userUpdate = await userService.asignRoleToUser(userId, roleId);
    if (!userUpdate) {
      res.status(404).json({
        message: `User with id ${userId} does not exist or role ${roleId} does not exist.`,
        status: 404,
      });
      return;
    }
    res.status(200).json(userUpdate);
  }

  return {
    findAll,
    create,
    deleteById,
    asignRoleToUser,
  };
}

module.exports = {
  usersController,
};
