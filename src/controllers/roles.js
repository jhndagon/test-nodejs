const { uuid } = require('uuidv4');

function rolesController(rolesService) {
  async function create(req, res) {
    const role = req.body;
    role.id = uuid();
    const roleInDb = await rolesService.create(role);
    if (!roleInDb) {
      return res.status(404).json({
        status: 404,
        message: 'Role name already exists in database.',
      });
    }
    return res.status(201).json(role);
  }

  return {
    create,
  };
}

module.exports = {
  rolesController,
};
