const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const { APP_PORT } = require('./config/environments');
const { sequelize, DataTypes, Op } = require('./config/database/sequelize');
const { route } = require('./router');

const app = express();
const router = express.Router();

app.use(morgan('tiny'));
app.use(bodyParser.json());

app.use('/api', route(router, sequelize, DataTypes, Op));

app.get('/api/test', (req, res) => {
  console.log('test');
  res.end('test');
});

app.listen(APP_PORT, () => {
  console.log(`Server executing in port : ${APP_PORT}`);
});
