require('dotenv').config();

module.exports = {
  DATABASE: process.env.POSTGRES_DATABASE || 'tres_pi_medios',
  USERNAME: process.env.POSTGRES_USERNAME || 'postgres',
  PASSWORD: process.env.POSTGRES_PASSWORD || 'postgres',
  HOST: process.env.POSTGRES_HOST || 'localhost',
  PORT: process.env.POSTGRES_PORT || '15432',
  APP_PORT: process.env.APP_PORT || 3000,
};
