const { Sequelize, DataTypes, Op } = require('sequelize');

const {
  USERNAME,
  PASSWORD,
  DATABASE,
  HOST,
  PORT,
} = require('../environments');

const sequelize = new Sequelize({
  dialect: 'postgres',
  username: USERNAME,
  password: PASSWORD,
  database: DATABASE,
  host: HOST,
  port: PORT,
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Database connected');
  })
  .catch((err) => {
    console.error('Database NOT connected', err);
  });

module.exports = {
  sequelize,
  DataTypes,
  Op,
};
