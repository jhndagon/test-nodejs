-- CREATE DATABASE tres_pi_medios;

CREATE TABLE IF NOT EXISTS roles (
    id uuid NOT NULL,
    name varchar(30) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users (
    id uuid NOT NULL,
    document varchar(20) NOT NULL,
    last_name varchar(30) NOT NULL,
    name varchar(30) NOT NULL,
    roles_id uuid NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (roles_id) REFERENCES roles(id)
);

CREATE TABLE IF NOT EXISTS products (
    id uuid NOT NULL,
    description varchar(30) NOT NULL,
    name varchar(30) NOT NULL,
    price int NOT NULL,
    
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS sales (
    id uuid NOT NULL,
    products_id uuid NOT NULL,
    qty int NOT NULL DEFAULT 1,
    sales_at timestamp without time zone NOT NULL DEFAULT (current_timestamp AT TIME ZONE 'America/Bogota'),
    users_id uuid NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (products_id) REFERENCES products(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

-- Insert products
INSERT INTO products(id, name, description, price) VALUES (
    '479fba0a-baaf-4b46-95a2-83a663817646', 
    'Arroz', 
    'Libra', 
    3000
);
INSERT INTO products(id, name, description, price) VALUES (
    'efbff7f6-6374-4c2f-9c96-3611c65068ba',
    'Papas',
    'Libra',
    1000
);
INSERT INTO products(id, name, description, price) VALUES (
    'f7c377cf-0f92-435a-b5e6-2c8cdd9d10c6',
    'Agua sin gas',
    '500 ml',
    2000
);
INSERT INTO products(id, name, description, price) VALUES (
    '3bed5d90-64ed-4bc1-8a3a-a378737ed542',
    'Agua con gas',
    '500 ml',
    2500
);
INSERT INTO products(id, name, description, price) VALUES (
    'c3f25f98-c5c3-4a00-b550-f716ae36b25f',
    'Docena de huevos',
    'ministro de haciendo aprueba',
    1800
);

-- Insert Roles
INSERT INTO roles(id, name) VALUES (
    '4b6c9866-3520-4863-94f1-7aa9c8fde997',
    'admin'
);
INSERT INTO roles(id, name) VALUES (
    '7f921861-8b18-43c2-84a6-baf398ebca2c',
    'employee'
);
INSERT INTO roles(id, name) VALUES (
    '29d120eb-f2ce-4c07-ad38-9cf818fb619c',
    'everyone'
);

-- Insert users
INSERT INTO users(id, document, last_name, name, roles_id) VALUES (
    '9bf1e41c-1a2d-449b-8f4c-ebfed5cae207',
    '12345',
    'Admin',
    'Juan',
    '4b6c9866-3520-4863-94f1-7aa9c8fde997'
);
INSERT INTO users(id, document, last_name, name, roles_id) VALUES (
    'cb29fba8-0874-4fca-88ba-b28c9f66f3ef',
    '54321',
    'Employee',
    'Santiago',
    '7f921861-8b18-43c2-84a6-baf398ebca2c'
);
INSERT INTO users(id, document, last_name, name, roles_id) VALUES (
    '5b2f8732-df9d-4f36-af3f-a2e96d0db409',
    '32415',
    'Everyone',
    'Pedro',
    '29d120eb-f2ce-4c07-ad38-9cf818fb619c'
);

