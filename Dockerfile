FROM node:16.17.1-alpine3.15
WORKDIR /usr/app
COPY package* ./
RUN npm install

COPY src /usr/app/src