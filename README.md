# Test to tres pi medios

## Steps
1. First of all sure you have **docker** and **docker-compose** installed.
2. execute comands from . (root project folder)
3. execute **docker network create tres-pi-medios**
4. run **docker-compose up**
	4.1. If there is any difficulty to execute te comando, the run **docker-compose up -d** postgres and
	4.2. in other terminal execute **docker-compose up web_service**, otherwise omit this 4.1 and 4.2 steps.
5. enjoy :rocket: testing the app.  

## Additional info

If you choose the option of execute the app with docker-compose

create file **.dev.env** with te environments

    POSTGRES_DATABASE=tres_pi_medios 
    POSTGRES_USERNAME=postgres
    POSTGRES_PASSWORD=postgres
    POSTGRES_PORT=5432
    POSTGRES_HOST=postgres
    APP_PORT=3000

otherwise create a **.env** adding the correspondent value

    POSTGRES_DATABASE=value1
    POSTGRES_USERNAME=value2
    POSTGRES_PASSWORD=value3
    POSTGRES_PORT=value4
    POSTGRES_HOST=value5
    APP_PORT=value6


## Notes
To export the endpoints in postman use the **postman.json** file, it can be find in the root folder project.